Machete

az login
az account list
az account set --subscription="ID"
az ad sp create-for-rbac --role="Contributor" --scopes="/subscription/ID"
az login --service-principal-name -u CLIENTID -p CLIENTPASSWORD --tenant TENANTID

terraform init
terraform plan -var-file vars-file.tf
terraform apply -var-file vars-file.tf
